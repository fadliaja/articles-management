

# ArticleApps

This project was generated using [Nx](https://nx.dev).

<p style="text-align: center;"><img src="https://raw.githubusercontent.com/nrwl/nx/master/images/nx-logo.png" width="450"></p>

🔎 **Smart, Fast and Extensible Build System**

## Guide

```bash
# install package
$ npm install

# after fill .env value based on .env.example, you can create the database using this command
$ npm run db:create

# run migration for migrate model to database
$ npm run db:migrate

# start application
$ npm run start

# running test
$ npm run test
```
