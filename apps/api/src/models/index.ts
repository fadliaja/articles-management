import { sequelizePostgres } from '../utils/dbConnection';
import News from './news.model';
import Topic from './topic.model';
import NewsTopic from './news-topic.model';

const models = [News, Topic, NewsTopic];
const modelInit = (): void => {
	models.forEach(model => {
		model.modelInit(sequelizePostgres);
	});

	models.forEach(model => {
		model.setAssociation();
	});
};

export default modelInit;
