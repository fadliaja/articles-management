import { DataTypes, Sequelize, Model, Optional, ModelAttributes, QueryInterface, Association } from "sequelize";
import NewsTopic from "./news-topic.model";
import Topic from "./topic.model";

export interface NewsAttributes {
  id: number;
  title: string;
  content: string;
  status: string;
  topics?: Topic[]; 
}

export type NewsCreationAttributes = Optional<NewsAttributes, "id">;

export class News extends Model<NewsAttributes, NewsCreationAttributes> implements NewsAttributes {
  public static readonly tableName = "MT_News";
	public static readonly modelName = "News";
	public static readonly modelNamePlural = "Newses";
	public static readonly defaultScope = {};
	public id!: number;
	public title!: string;
  public content!: string;
  public status!: string;
	public readonly createdAt?: Date;
	public readonly updatedAt?: Date;
	public readonly deletedAt?: Date;

  public topics: Topic[];

  public static associations: {
    topics: Association<News, Topic>
  };

  public static setAssociation(): void {
    this.belongsToMany(Topic, {
      through: NewsTopic.tableName,
      foreignKey: 'newsId',
      as: 'topics',
      otherKey: 'topicId',
      timestamps: false
    })
  }

  public static tableDefinitions: ModelAttributes<News, NewsAttributes> = {
    id: {
      type: new DataTypes.INTEGER(),
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    title: new DataTypes.STRING(),
    content: new DataTypes.TEXT(),
    status: new DataTypes.ENUM('DRAFT', 'DELETED', 'PUBLISHED')
  }

  public static modelInit(sequlize: Sequelize): void {
    this.init(this.tableDefinitions,
      {
        sequelize: sequlize,
        tableName: this.tableName,
        name: {
          singular: this.modelName,
          plural: this.modelNamePlural
        },
        defaultScope: this.defaultScope,
        comment: "Model for the accessible data of News",
        paranoid: true
      }
    )
  }

  public static async createTable(query: QueryInterface): Promise<void> {
    await query.createTable(this.tableName, {
      ...this.tableDefinitions,
      createdAt: {
        type: DataTypes.DATE
      },
      updatedAt: {
        type: DataTypes.DATE
      },
      deletedAt: {
        type: DataTypes.DATE
      },
    })
  }

  public static async dropTable(query: QueryInterface): Promise<void> {
    await query.dropTable(this.tableName, { force: false })
  }
}

export default News;