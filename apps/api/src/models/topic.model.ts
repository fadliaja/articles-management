import { DataTypes, Sequelize, Model, Optional, ModelAttributes, QueryInterface, Association } from "sequelize";
import NewsTopic from "./news-topic.model";
import News from "./news.model";

export interface TopicAttributes {
  id: number;
  name: string;
}

export type TopicCreationAttributes = Optional<TopicAttributes, "id">;

export class Topic extends Model<TopicAttributes, TopicCreationAttributes> implements TopicAttributes {
  public static readonly tableName = "MT_Topic";
	public static readonly modelName = "Topic";
	public static readonly modelNamePlural = "Topics";
	public static readonly defaultScope = {};
	public id!: number;
	public name!: string;
	public readonly createdAt?: Date;
	public readonly updatedAt?: Date;
	public readonly deletedAt?: Date;

  public newses: News[];

  public static associations: {
    newses: Association<Topic, News>
  };

  public static setAssociation(): void {
    this.belongsToMany(News, {
      through: NewsTopic.tableName,
      foreignKey: 'topicId',
      as: 'newses',
      otherKey: 'newsId',
      timestamps: false
    })
  }

  public static tableDefinitions: ModelAttributes<Topic, TopicAttributes> = {
    id: {
      type: new DataTypes.INTEGER(),
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    name: new DataTypes.STRING(),
  }

  public static modelInit(sequlize: Sequelize): void {
    this.init(this.tableDefinitions,
      {
        sequelize: sequlize,
        tableName: this.tableName,
        name: {
          singular: this.modelName,
          plural: this.modelNamePlural
        },
        defaultScope: this.defaultScope,
        comment: "Model for the accessible data of Topic",
        paranoid: true
      }
    )
  }

  public static async createTable(query: QueryInterface): Promise<void> {
    await query.createTable(this.tableName, {
      ...this.tableDefinitions,
      createdAt: {
        type: DataTypes.DATE
      },
      updatedAt: {
        type: DataTypes.DATE
      },
      deletedAt: {
        type: DataTypes.DATE
      },
    })
  }

  public static async dropTable(query: QueryInterface): Promise<void> {
    await query.dropTable(this.tableName, { force: false })
  }
}

export default Topic;