import { DataTypes, Sequelize, Model, ModelAttributes, QueryInterface, Association } from "sequelize";
import News from "./news.model";
import Topic from "./topic.model";

export interface NewsTopicAttributes {
  id: number;
  newsId: number;
  topicId: number;
}

export type NewsTopicCreationAttributes = NewsTopicAttributes;

export class NewsTopic extends Model<NewsTopicAttributes, NewsTopicCreationAttributes> implements NewsTopicAttributes {
  public static readonly tableName = "Pivot_NewsTopic";
	public static readonly modelName = "NewsTopic";
	public static readonly modelNamePlural = "NewsTopics";
	public static readonly defaultScope = {};
	public id!: number;
	public newsId!: number;
  public topicId!: number;

  public news: News;
  public topic: Topic;

  public static associations: {
    news: Association<NewsTopic, News>,
    topic: Association<NewsTopic, Topic>
  };

  public static setAssociation(): void {
    this.belongsTo(News, { foreignKey: 'newsId', targetKey: 'id', as: 'news' });
    this.belongsTo(Topic, { foreignKey: 'topicId', targetKey: 'id', as: 'topic' });
  }

  public static tableDefinitions: ModelAttributes<NewsTopic, NewsTopicAttributes> = {
    id: {
      type: new DataTypes.INTEGER(),
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    newsId: new DataTypes.INTEGER(),
    topicId: new DataTypes.INTEGER()
  }

  public static modelInit(sequlize: Sequelize): void {
    this.init(this.tableDefinitions,
      {
        sequelize: sequlize,
        tableName: this.tableName,
        name: {
          singular: this.modelName,
          plural: this.modelNamePlural
        },
        defaultScope: this.defaultScope,
        comment: "Model for the accessible data of NewsTopic",
        timestamps: false
      }
    )
  }

  public static async createTable(query: QueryInterface): Promise<void> {
    await query.createTable(this.tableName, { ...this.tableDefinitions })
  }

  public static async dropTable(query: QueryInterface): Promise<void> {
    await query.dropTable(this.tableName, { force: false })
  }
}

export default NewsTopic;