export interface IPaginationBase {
  pagination?: number;
  limit?: number;
}

export interface IRequestParams {
  id: number;
}