import { IPaginationBase } from "./base.interface";

export type ITopicQuery = IPaginationBase

export interface ITopicBody {
  name: string;
}