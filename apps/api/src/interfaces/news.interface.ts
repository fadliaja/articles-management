import { IPaginationBase } from "./base.interface";

export interface INewsQuery extends IPaginationBase {
  status: string;
  topic_id: number;
}

export interface INewsBody {
  title: string;
  content: string;
  status: string;
  topic_ids: Array<number>;
}

