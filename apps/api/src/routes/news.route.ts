import { FastifyInstance } from "fastify";
import { createNews, deleteNews, detailNews, getAllNews, updateNews } from '../controllers/news.controller';

const newsRoute = async (fastify: FastifyInstance) => {
  fastify.get('/newses', getAllNews);
  fastify.post('/newses', createNews);
  fastify.get('/newses/:id', detailNews);
  fastify.put('/newses/:id', updateNews);
  fastify.delete('/newses/:id', deleteNews);
}

export default newsRoute;