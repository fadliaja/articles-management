import { FastifyInstance } from "fastify";
import { createTopic, deleteTopic, detailTopic, getAllTopic, updateTopic } from '../controllers/topic.controller';

const topicRoute = async (fastify: FastifyInstance) => {
  fastify.get('/topics', getAllTopic);
  fastify.post('/topics', createTopic);
  fastify.get('/topics/:id', detailTopic);
  fastify.put('/topics/:id', updateTopic);
  fastify.delete('/topics/:id', deleteTopic);
}

export default topicRoute;