import { FastifyRequest, FastifyReply } from 'fastify';
import { FindAndCountOptions, Op, WhereOptions } from 'sequelize';
import { INewsBody, INewsQuery, IRequestParams } from '../interfaces';
import NewsTopic from '../models/news-topic.model';
import News from "../models/news.model";
import Topic from '../models/topic.model';
import { createNewsService, deleteNewsService, findAllNewsService, findNewsService, updateNewsService } from '../services/news.service';

export const getAllNews = async (request: FastifyRequest, reply: FastifyReply) => {
  try {
    const { status, topic_id: topicId, pagination, limit } = request.query as INewsQuery;

    const topicWhere: WhereOptions<Topic> = {};
    const where: WhereOptions<News> = {};

    const query: FindAndCountOptions = {};

    if (pagination && limit) {
      query.limit = +limit;
      query.offset = (+pagination - 1) * +limit;
    } 

    if (status) where.status = status;
    if (topicId) topicWhere.id = topicId;

    query.where = where;
    query.include = [
      {
        association: News.associations.topics,
        as: 'topics',
        where: topicWhere,
        required: false
      }
    ];

    const { success, message, data, meta } = await findAllNewsService(query, { pagination, limit })

    reply.send({ success, message, data, meta });
  } catch (error) {
    reply.status(500).send({
      success: false,
      message: error?.message ?? error
    });
  }
}

export const createNews = async (request: FastifyRequest, reply: FastifyReply) => {
  try {
    const { title, content, status, topic_ids: topicIds } = request.body as INewsBody;

    if (!Array.isArray(topicIds)) throw new Error('Topic ids must be an array');

    const { success, message, data } = await createNewsService({ title, content, status });

    if (!success) throw new Error(message);
    
    const newsTopics = topicIds.map(topicId => ({ newsId: data.id, topicId: topicId }));
    await NewsTopic.bulkCreate(newsTopics);

    reply.send({ success, message, data });
  } catch (error) {
    reply.status(500).send({
      success: false,
      message: error?.message ?? error
    });
  }
}

export const detailNews = async (request: FastifyRequest, reply: FastifyReply) => {
  try {
    const { id } = request.params as IRequestParams;

    const { success, message, data } = await findNewsService(id);

    if (!success) throw new Error(message);

    reply.send({ success, message, data });
  } catch (error) {
    reply.status(500).send({
      success: false,
      message: error?.message ?? error
    });
  }
}

export const updateNews = async (request: FastifyRequest, reply: FastifyReply) => {
  try {
    const { id } = request.params as IRequestParams;
    const { title, content, status, topic_ids: topicIds } = request.body as INewsBody;

    let { success, message, data } = await findNewsService(id);

    if (!success) throw new Error(message);

    if (Array.isArray(topicIds)) {
      const currentTopicIds = data.topics.map(topic => topic.id);

      const deletedTopicIds = currentTopicIds.filter(topicId => !topicIds.includes(topicId));
      const addedTopicIds = (topicIds as Array<number>).filter(topicId => !currentTopicIds.includes(topicId));

      if (deletedTopicIds.length > 0) await NewsTopic.destroy({
        where: { 
          newsId: data.id,
          topicId: { [Op.in]: deletedTopicIds }
        } 
      });
      
      if (addedTopicIds.length > 0) {
        const newsTopicBodies = addedTopicIds.map(topicId => ({ newsId: data.id, topicId }));
        await NewsTopic.bulkCreate(newsTopicBodies);
      }
    }
    ({ success, message, data } = await updateNewsService(id, { title, content, status }));

    if (!success) throw new Error(message);

    reply.send({ success, message, data });
  } catch (error) {
    reply.status(500).send({
      success: false,
      message: error?.message ?? error
    });
  }
}

export const deleteNews = async (request: FastifyRequest, reply: FastifyReply) => {
  try {
    const { id } = request.params as IRequestParams;

    let { success, message, data } = await findNewsService(id);

    if (!success) throw new Error(message);

    ({ success, message, data } = await deleteNewsService(id));

    if (!success) throw new Error(message);

    reply.send({ success, message, data: null });
  } catch (error) {
    reply.status(500).send({
      success: false,
      message: error?.message ?? error
    });
  }
}