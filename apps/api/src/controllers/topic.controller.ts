import { FastifyRequest, FastifyReply } from 'fastify';
import { FindAndCountOptions } from 'sequelize/types';
import { IRequestParams, ITopicBody, ITopicQuery } from '../interfaces';
import { createTopicService, deleteTopicService, findAllTopicService, findTopicService, updateTopicService } from '../services/topic.service';

export const getAllTopic = async (request: FastifyRequest, reply: FastifyReply) => {
  try {
    const { pagination, limit } = request.query as ITopicQuery;
    const query: FindAndCountOptions = {};
    
    if (pagination && limit) {
      query.limit = +limit;
      query.offset = (+pagination - 1) * +limit;
    }

    const { success, message, data, meta } = await findAllTopicService(query, { pagination, limit });

    if (!success) throw new Error(message);

    reply.send({ success, message, data, meta });
  } catch (error) {
    reply.status(500).send({
      success: false,
      data: null,
      message: error?.message ?? error
    });
  }
}

export const createTopic = async (request: FastifyRequest, reply: FastifyReply) => {
  try {
    const { name } = request.body as ITopicBody;

    const { success, message, data } = await createTopicService({ name });

    if (!success) throw new Error(message);

    reply.send({ success, message, data });
  } catch (error) {
    reply.status(500).send({
      success: false,
      data: null,
      message: error?.message ?? error
    });
  }
}

export const detailTopic = async (request: FastifyRequest, reply: FastifyReply) => {
  try {
    const { id } = request.params as IRequestParams;
    const { success, message, data } = await findTopicService(id);

    if (!success) throw new Error(message);

    reply.send({ success, message, data })
  } catch (error) {
    reply.status(500).send({
      success: false,
      message: error?.message ?? error
    });
  }
}

export const updateTopic = async (request: FastifyRequest, reply: FastifyReply) => {
  try {
    const { id } = request.params as IRequestParams;
    const { name } = request.body as ITopicBody;

    let { success, message, data } = await findTopicService(id);

    if (!success) throw new Error(message);
    
    ({ success, message, data } = await updateTopicService(id, { name }))

    if (!success) throw new Error(message);

    reply.send({ success, message, data });
  } catch (error) {
    reply.status(500).send({
      success: false,
      message: error?.message ?? error
    });
  }
}

export const deleteTopic = async (request: FastifyRequest, reply: FastifyReply) => {
  try {
    const { id } = request.params as IRequestParams;

    let { success, message, data } = await findTopicService(id);

    if (!success) throw new Error(message);

    ({ success, message, data } = await deleteTopicService(id))

    reply.send({ success, message, data: null });
  } catch (error) {
    reply.status(500).send({
      success: false,
      message: error?.message ?? error
    });
  }
}