// eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
import app from 'apps/api/src/main';
import * as request from 'supertest';
import modelInit from '../models';
import Topic from '../models/topic.model';

describe("CRUD Topic", () => {
  let topicId = null;

  beforeAll(async () => {
    modelInit();
    await app.ready();

    await Topic.destroy({
      where: {
        name: ['Test Name', 'Test Name Ganti'],
      },
    });
  });

  test("Create Topics", async () => {
    const res = await request(app.server)
      .post("/topics")
      .send({
        name: 'Test Name',
      });

    expect(res.status).toBe(200);
    expect(res.body).toHaveProperty('success', true);
    expect(res.body).toHaveProperty('data');
    expect(res.body.data).toHaveProperty('name');

    topicId = res.body.data.id;
  });

  test("Get Detail Topics", async() => {
    expect(topicId).not.toBeNull();
    expect(topicId).not.toBeUndefined();

    const res = await request(app.server).get(`/topics/${topicId}`);
    expect(res.status).toBe(200);
    expect(res.body).toHaveProperty('success', true);
    expect(res.body).toHaveProperty('data');
    expect(res.body.data).toHaveProperty('name');
  });

  test("Get All Topics", async() => {
    const res = await request(app.server).get("/topics");
    expect(res.status).toBe(200);
    expect(res.body).toHaveProperty('success', true);
    expect(res.body).toHaveProperty('data');
    expect(res.body.data).toEqual(expect.any(Array));
  });

  test("Update Topics", async() => {
    expect(topicId).not.toBeNull();
    expect(topicId).not.toBeUndefined();

    const res = await request(app.server)
      .put(`/topics/${topicId}`)
      .send({
        name: 'Test Name Ganti'
      });
    expect(res.status).toBe(200);
    expect(res.body).toHaveProperty('success', true);
    expect(res.body).toHaveProperty('data');
    expect(res.body.data).toHaveProperty('name');
  });

  test("Delete Topics", async() => {
    expect(topicId).not.toBeNull();
    expect(topicId).not.toBeUndefined();

    const res = await request(app.server)
      .delete(`/topics/${topicId}`);
    expect(res.status).toBe(200);
    expect(res.body).toHaveProperty('success', true);
  });

  afterAll(async () => {
    await app.close();
  });
  
});