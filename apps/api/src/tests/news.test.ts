// eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
import app from 'apps/api/src/main';
import * as request from 'supertest';
import modelInit from '../models';
import Topic from '../models/topic.model';

describe("CRUD News", () => {
  let topicId = null;
  let newsId = null;

  beforeAll(async () => {
    modelInit();
    await app.ready();

    const topic = await Topic.create({
      name: 'Test Topic'
    });
    topicId = topic.id;
  });

  test("Create Newses", async () => {
    expect(topicId).not.toBeNull();
    expect(topicId).not.toBeUndefined();

    const res = await request(app.server)
      .post("/newses")
      .send({
        title: 'Test News',
        content: 'This is Test News Content',
        status: 'DRAFT',
        topic_ids: [topicId]
      });
      
    expect(res.status).toBe(200);
    expect(res.body).toHaveProperty('success', true);
    expect(res.body).toHaveProperty('data');
    expect(res.body.data).toHaveProperty('title');
    expect(res.body.data).toHaveProperty('content');
    expect(res.body.data).toHaveProperty('status');

    newsId = res.body.data.id;
  });

  test("Get Detail Newses", async() => {
    expect(newsId).not.toBeNull();
    expect(newsId).not.toBeUndefined();

    const res = await request(app.server).get(`/newses/${newsId}`);
    expect(res.status).toBe(200);
    expect(res.body).toHaveProperty('success', true);
    expect(res.body).toHaveProperty('data');
    expect(res.body.data).toHaveProperty('title');
    expect(res.body.data).toHaveProperty('content');
    expect(res.body.data).toHaveProperty('status');
  });

  test("Get All Newses", async() => {
    const res = await request(app.server).get("/newses");
    expect(res.status).toBe(200);
    expect(res.body).toHaveProperty('success', true);
    expect(res.body).toHaveProperty('data');
    expect(res.body.data).toEqual(expect.any(Array));
  });

  test("Update Newses", async() => {
    expect(newsId).not.toBeNull();
    expect(newsId).not.toBeUndefined();

    const res = await request(app.server)
      .put(`/newses/${newsId}`)
      .send({
        title: 'Test News Ganti',
        content: 'This is Test News Content Ganti',
        status: 'PUBLISHED'
      });
    expect(res.status).toBe(200);
    expect(res.body).toHaveProperty('success', true);
    expect(res.body).toHaveProperty('data');
    expect(res.body.data).toHaveProperty('title');
    expect(res.body.data).toHaveProperty('content');
    expect(res.body.data).toHaveProperty('status');
  });

  test("Delete Newses", async() => {
    expect(newsId).not.toBeNull();
    expect(newsId).not.toBeUndefined();

    const res = await request(app.server)
      .delete(`/newses/${newsId}`);
    expect(res.status).toBe(200);
    expect(res.body).toHaveProperty('success', true);
  });

  afterAll(async () => {
    await Topic.destroy({
      where: {
        id: topicId,
      },
    });
    await app.close();
  });
  
});