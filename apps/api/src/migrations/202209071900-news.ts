import { QueryInterface } from "sequelize";
import News from "../models/news.model";

export const up = async (query: QueryInterface): Promise<void> => {
	try {
		return News.createTable(query);
	} catch (error) {
		return Promise.reject(error);
	}
};

export const down = async (query: QueryInterface): Promise<void> => {
	try {
		return News.dropTable(query);
	} catch (error) {
		return Promise.reject(error);
	}
};
