import { QueryInterface } from "sequelize";
import Topic from "../models/topic.model";

export const up = async (query: QueryInterface): Promise<void> => {
	try {
		return Topic.createTable(query);
	} catch (error) {
		return Promise.reject(error);
	}
};

export const down = async (query: QueryInterface): Promise<void> => {
	try {
		return Topic.dropTable(query);
	} catch (error) {
		return Promise.reject(error);
	}
};
