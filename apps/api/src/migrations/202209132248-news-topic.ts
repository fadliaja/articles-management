import { QueryInterface } from "sequelize";
import NewsTopic from "../models/news-topic.model";

export const up = async (query: QueryInterface): Promise<void> => {
	try {
		return NewsTopic.createTable(query);
	} catch (error) {
		return Promise.reject(error);
	}
};

export const down = async (query: QueryInterface): Promise<void> => {
	try {
		return NewsTopic.dropTable(query);
	} catch (error) {
		return Promise.reject(error);
	}
};
