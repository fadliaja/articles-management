import { Options } from "sequelize";

interface _Options extends Options {
	seederStorage?: "json" | "sequelize";
}

const {
	DB_USERNAME,
	DB_PASSWORD,
	DB_HOST,
	DB_PORT,
	DB_NAME,
	DB_DIALECT,
} = process.env;

export const development: _Options = {
	username: DB_USERNAME,
	password: DB_PASSWORD,
	host: DB_HOST,
	port: +DB_PORT,
	database: DB_NAME,
	dialect: DB_DIALECT ? "postgres" : "postgres",
	seederStorage: "sequelize",
};
