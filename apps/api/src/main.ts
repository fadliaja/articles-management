import fastify from "fastify";
import modelInit from "./models";
import newsRoute from "./routes/news.route" ;
import topicRoute from "./routes/topic.route";
const { PORT } = process.env;

const server = fastify();

server.register(newsRoute);
server.register(topicRoute);

server.listen({ port: +PORT || 8080 }, async (err, address) => {
  if (err) {
    console.log(err);
    process.exit(1)
  }
  await modelInit();
  console.log(`Server listening at ${address}`);
})

export default server;