import { FindAndCountOptions } from "sequelize";
import { IPaginationBase } from "../interfaces";
import Topic, { TopicCreationAttributes } from "../models/topic.model";

export const findAllTopicService = async (query: FindAndCountOptions, properties: IPaginationBase) => {
  try {
    const { pagination, limit } = properties;
    const topic = await Topic.findAndCountAll(query);

    const meta = {
      page: pagination ? +pagination : null,
      limit: limit ? +limit : null,
      totalPages: limit ? Math.ceil(topic.count / limit) : null,
      count: topic.count,
    }

    return { success: true, message: 'Successfully Get All Topics', data: topic.rows, meta };
  } catch (error) {
    return {
      success: false,
      message: error?.messsage ?? error,
      data: null
    }
  }
}

export const createTopicService = async (body: TopicCreationAttributes) => {
  try {
    const topic = await Topic.create(body);

    return { success: true, message: 'Successfully Create Topic', data: topic };
  } catch (error) {
    return {
      success: false,
      message: error?.messsage ?? error,
      data: null
    }
  }
}

export const findTopicService = async (id: number) => {
  try {
    const topic = await Topic.findOne({
      where: {
        id
      },
      include: [
        {
          association: Topic.associations.newses,
          as: 'newses'
        }
      ]
    });

    if (!topic) throw new Error(`Topic with ID ${id} not found`);

    return { success: true, message: 'Successfully Get Detail Topic', data: topic };
  } catch (error) {
    return {
      success: false,
      message: error?.messsage ?? error,
      data: null
    }
  }
}

export const updateTopicService = async (id: number, body: TopicCreationAttributes) => {
  try {
    const topic = await Topic.update(body, {
      where: {
        id
      },
      returning: true
    });

    return { success: true, message: 'Successfully Update Topic', data: topic[1] ? topic[1][0] ?? null : null };
  } catch (error) {
    return {
      success: false,
      message: error?.messsage ?? error,
      data: null
    }
  }
}

export const deleteTopicService = async (id: number) => {
  try {
    const topic = await Topic.destroy({
      where: {
        id
      }
    });

    return { success: true, message: 'Successfully Delete Topic', data: topic };
  } catch (error) {
    return {
      success: false,
      message: error?.messsage ?? error,
      data: null
    }
  }
}