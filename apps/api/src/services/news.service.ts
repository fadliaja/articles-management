import { FindAndCountOptions } from "sequelize";
import { IPaginationBase } from "../interfaces";
import News, { NewsCreationAttributes } from "../models/news.model";

export const findAllNewsService = async (query: FindAndCountOptions, properties: IPaginationBase) => {
  try {
    const { pagination, limit } = properties;
    const news = await News.findAndCountAll(query);

    const meta = {
      page: pagination ? +pagination : null,
      limit: limit ? +limit : null,
      totalPages: limit ? Math.ceil(news.count / limit) : null,
      count: news.count,
    }

    return { success: true, message: 'Successfully Get All Newss', data: news.rows, meta };
  } catch (error) {
    return {
      success: false,
      message: error?.messsage ?? error,
      data: null
    }
  }
}

export const createNewsService = async (body: NewsCreationAttributes) => {
  try {
    const news = await News.create(body);

    return { success: true, message: 'Successfully Create News', data: news };
  } catch (error) {
    return {
      success: false,
      message: error?.messsage ?? error,
      data: null
    }
  }
}

export const findNewsService = async (id: number) => {
  try {
    const news = await News.findOne({
      where: {
        id
      },
      include: [
        {
          attributes: ['id', 'name'],
          association: News.associations.topics,
          as: 'topics'
        }
      ]
    });

    if (!news) throw new Error(`News with ID ${id} not found`);

    return { success: true, message: 'Successfully Get Detail News', data: news };
  } catch (error) {
    return {
      success: false,
      message: error?.messsage ?? error,
      data: null
    }
  }
}

export const updateNewsService = async (id: number, body: NewsCreationAttributes) => {
  try {
    const news = await News.update(body, {
      where: {
        id
      },
      returning: true
    });

    return { success: true, message: 'Successfully Update News', data: news[1] ? news[1][0] ?? null : null };
  } catch (error) {
    return {
      success: false,
      message: error?.messsage ?? error,
      data: null
    }
  }
}

export const deleteNewsService = async (id: number) => {
  try {
    const news = await News.destroy({
      where: {
        id
      }
    });

    return { success: true, message: 'Successfully Delete News', data: news };
  } catch (error) {
    return {
      success: false,
      message: error?.messsage ?? error,
      data: null
    }
  }
}